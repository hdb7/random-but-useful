# List of useful one-line script

## List the all installed packages on Debian-based distro

```
  apt list --installed | awk '{print $1}'| cut -f1 -d"/"
```
## Display the connected wifi name  

```
  iw dev | grep ssid | awk '{print $2}'
```
